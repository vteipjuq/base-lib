# BaseLib

#### 介绍

BaseLib 基础库常用的函数和类库,40个常用的函数。参数验证和异常抛出基础函数。



#### 本插件只支持laravel



#### 安装教程

<br>

安装


[//]: # (我是注释)

```

添加指定包 composer require raymond/baselib 

更新指定包 composer update raymond/baselib

移除指定的包 composer remove raymond/baselib

或删除vendor目录中的raymond

添加测式包：composer require "raymond/baselib:dev-master" -vvv

```


<br>
发布配置文件到config目录下面(可选):
```
php artisan vendor:publish
```

<br>

config/app.php中加入如下代码：
```
'providers' => [
	Raymond\BaseLib\BaseLibServiceProvider::class,
]

'aliases' => [
	'BaseLib' => Raymond\BaseLib\BaseLib::class,
]
```

<br>

控制器中引用:

```
use BaseLib;
```
<br/>

#### 使用说明

###### 常用函数
> 调用方法：类名::函数名 ，也可以BaseLib::common()->getlasttime()这种方法调用也可以

```

例：BaseLib::common()->getlasttime('2020-10-09 09:14');

```
<br/>

> 计算时间: BaseLib::get_last_time(2020-10-09 09:14);

名称 | 是否必须 | 备注
:---: | :---: | :---:
string | true | 日期字符串（例：2020-10-09 09:14）

<br/>

> 手机号码验证: BaseLib::is_phone(15610032203);

名称 | 是否必须 | 备注
:---: | :---: | :---:
string | true | 手机号（例：15610032203）

<br/>

> 手机号隐藏中间4位: BaseLib::substr_mobile(15610032203)

名称 | 是否必须 | 备注
:---: | :---: | :---:
string | true | 手机号（例：15610032203）

<br/>

> curl get 请求: BaseLib::http_get(url);

名称 | 是否必须 | 备注
:---: | :---: | :---:
url | true | url（例：http://www.baidu.com）

<br/>

> curl post 请求: BaseLib::http_post(url,data);

名称 | 是否必须 | 备注
:---: | :---: | :---:
url | true | url（例：http://www.baidu.com）
data | true | 数组参数['id'=>1,'name'=>'张飞']

<br/>

> 对emoji表情转义: BaseLib::emoji_encode();

名称 | 是否必须 | 备注
:---: | :---: | :---:
string | true | 表情字符串

<br/>

> 对emoji表情转反义: BaseLib::emoji_decode();

名称 | 是否必须 | 备注
:---: | :---: | :---:
string | true | 表情字符串

<br/>

> 判断字符串是否全是英文: BaseLib::is_english();

名称 | 是否必须 | 备注
:---: | :---: | :---:
string | true | 字符串

<br/>

> 字符串截取并且超出显示省略号: BaseLib::subtext();

名称 | 是否必须 | 备注
:---: | :---: | :---:
text | true | 字符串
length | false | 字符串长度

<br/>

> 字符串截取: BaseLib::get_str_truncate();

名称 | 是否必须 | 备注
:---: | :---: | :---:
text | true | 字符串
length | false | 字符串长度
etc | false | 要替换的字符串

<br/>

> 获取随机字符串: BaseLib::random();

名称 | 是否必须 | 备注
:---: | :---: | :---:
length | false | 字符串长度
type | false | 类型
convert | false | 转换大小写 1大写 0小写

<br/>

> 判断数组类型参数是否含有空元素值: BaseLib::is_arraynull();

名称 | 是否必须 | 备注
:---: | :---: | :---:
arr | true | 数组

<br/>

> utf-8转gbk: BaseLib::gbk();

名称 | 是否必须 | 备注
:---: | :---: | :---:
str | true | 字符串

<br/>

> gbk转utf8: BaseLib::utf8();

名称 | 是否必须 | 备注
:---: | :---: | :---:
str | true | 字符串

<br/>

> 正则验证邮箱地址: BaseLib::match_email()

名称 | 是否必须 | 备注
:---: | :---: | :---:
email | true | 邮箱

<br/>

> 查询字符是否存在于某字符串: BaseLib::str_exists()

名称 | 是否必须 | 备注
:---: | :---: | :---:
haystack | true | 字符串
needle | true | 要查找的字符

<br/>

> 检测输入中是否含有特殊字符: BaseLib::is_badword()

名称 | 是否必须 | 备注
:---: | :---: | :---:
string | true | 字符串

<br/>

> 处理json字符中的特殊字符: BaseLib::get_json_to_arr()

名称 | 是否必须 | 备注
:---: | :---: | :---:
string | true | json字符串

<br/>

> 非法字符过滤函数: BaseLib::has_unsafeword()

名称 | 是否必须 | 备注
:---: | :---: | :---:
string | true | 字符串

<br/>

> 判断是否是微信浏览器还是企业微信浏览器: BaseLib::is_wxbrowser()

名称 | 是否必须 | 备注
:---: | :---: | :---:
无 | 无 | 无

<br/>

> 判断是否是企业微信浏览器: BaseLib::is_wx_company()

名称 | 是否必须 | 备注
:---: | :---: | :---:
无 | 无 | 无

<br/>

> 判断两个字符串是否相等: BaseLib::string_equal()

名称 | 是否必须 | 备注
:---: | :---: | :---:
str1 | true | 字符串
str2 | true | 字符串

<br/>

> 随机验证码: BaseLib::get_random()

名称 | 是否必须 | 备注
:---: | :---: | :---:
length | false | 生成随机数长度，默认为6

<br/>

> 压缩文件: BaseLib::zip()

名称 | 是否必须 | 备注
:---: | :---: | :---:
files | true | 待压缩文件
filePath | true |  输出文件路径 【绝对文件地址】

<br/>

> zip解压方法: BaseLib::unzip()

名称 | 是否必须 | 备注
:---: | :---: | :---:
filePath | true | 压缩包所在地址 【绝对文件地址】
path | true |  解压路径 【绝对文件目录路径】

<br/>

> 二维数组去重: BaseLib::array_unset_repeat()

名称 | 是否必须 | 备注
:---: | :---: | :---:
arr | true | 数组
key | true |  判断的键值

<br/>

> 二维数组合并重复项: BaseLib::array_restruct()

名称 | 是否必须 | 备注
:---: | :---: | :---:
arr | true | 数组
key | true |  判断的键值

<br/>

> 判断字符串是否在数组中: BaseLib::for_in_array()

名称 | 是否必须 | 备注
:---: | :---: | :---:
str | true |  判定的字符串
arr | true | 数组

<br/>

> 获取客户端IP: BaseLib::get_ip()

名称 | 是否必须 | 备注
:---: | :---: | :---:
无 | 无 | 无

<br/>

> 自动生成密码: BaseLib::generate_password()

名称 | 是否必须 | 备注
:---: | :---: | :---:
length | true |  密码长度
strength | true | 1:加入大写字母BDGHJLMNPQRSTVWXZ，2：加入AEUY，4：加入数字，8：加入特殊字符'@#$%'

<br/>

> 检查密码长度是否符合规定: BaseLib::is_password()

名称 | 是否必须 | 备注
:---: | :---: | :---:
password | true |  密码

<br/>

> 人民币数字小写转大写: BaseLib::rmb_format()

名称 | 是否必须 | 备注
:---: | :---: | :---:
money | true |  人民币数值
int_unit | false |  币种单位，默认"元"，有的需求可能为"圆"
is_round | false |  是否对小数进行四舍五入
is_extra_zero | false |  是否对整数部分以0结尾，小数存在的数字附加0,比如1960.30

<br/>

> 返回格式化数字: BaseLib::number_format_plus()

名称 | 是否必须 | 备注
:---: | :---: | :---:
number | true |  待格式化数字
decimals | false |  保留小数位数，默认2位
dec_point | false |  整数和小数分隔符号
thousands_sep | false |  整数部分每三位数读分隔符号

<br/>

> 转换时间戳为常用的日期格式: BaseLib::trans_time()

名称 | 是否必须 | 备注
:---: | :---: | :---:
timestamp | true |  时间戳

<br/>

> 银行卡号格式检测: BaseLib::chk_card()

名称 | 是否必须 | 备注
:---: | :---: | :---:
card | true |  卡号

<br/>

> 根据身份证号计算性别: BaseLib::get_gender()

名称 | 是否必须 | 备注
:---: | :---: | :---:
id | true |  身份证号

<br/>

> 检测姓名中文格式: BaseLib::chk_chinese()

名称 | 是否必须 | 备注
:---: | :---: | :---:
name | true |  字符串

<br/>

> 将数组转换成xml: BaseLib::arr_xml()

名称 | 是否必须 | 备注
:---: | :---: | :---:
arr | true |  数组

<br/>

> 将xml转为array: BaseLib::xml_arr()

名称 | 是否必须 | 备注
:---: | :---: | :---:
xml | true |  xml字符串

<br/>

> 根据过期时间判断剩余的天数: BaseLib::check_remaining_days()

名称 | 是否必须 | 备注
:---: | :---: | :---:
expire_time | true |  时间戳

<br/>

> 数组编码转换: BaseLib::arr_iconv()

名称 | 是否必须 | 备注
:---: | :---: | :---:
arr | true |  待处理的数组
in_charset | true |  输入编码
out_charset | true |  输出编码

<br/>

> 获取文件扩展名（后缀）: BaseLib::get_extension()

名称 | 是否必须 | 备注
:---: | :---: | :---:
filename | true |  文件名

<br/>

> 列出目录下的文件名: BaseLib::listDirFiles()

名称 | 是否必须 | 备注
:---: | :---: | :---:
DirPath | true |  目录

<br/>

> 检查目标文件夹是否存在，如果不存在则自动创建该目录: BaseLib::make_dir()

名称 | 是否必须 | 备注
:---: | :---: | :---:
folder | true |  目录路径。不能使用相对于网站根目录的URL

<br/>

> 滤器laravel request中请求参数的数组中的多余参数: BaseLib::params_filter()

名称 | 是否必须 | 备注
:---: | :---: | :---:
request | true |  数组
key | false |  要删除的数组下标（可以不传）例：['id','name']

<br/>

##### 生成海报: BaseLib::create_poster()

名称 | 是否必须 | 备注
:---: | :---: | :---:
config | true |  海报配置信息，类型为数组
filename | true |  海报保存路径

> config

名称 | 是否必须 | 备注
:---: | :---: | :---:
text | false |  文字，类型为数组
image | false |  图片，类型为数组
background | true |  背景图，类型为数组

> text

名称 | 是否必须 | 备注
:---: | :---: | :---:
text | true |  文字
left | false |  左边距
top | false |  上边距
fontSize | false |  字号
fontColor | false |  字体颜色
fontPath | false |  字体路径

> image

名称 | 是否必须 | 备注
:---: | :---: | :---:
url | true |  图地址
width | false |  宽度
height | false |  高度
left | false |  左边距
right | false |  右边距
top | false |  上边距
bottom | false |  下边距
opacity | false |  透明度
radius | false |  圆角

> background

名称 | 是否必须 | 备注
:---: | :---: | :---:
url | true |  背景图地址
radius | false |  圆角

```
	$config = [
		//文字
		'text'=>[
			['text'=>'昵称','left'=>182,'top'=>105,'fontSize'=>18,'fontColor'=>'255,0,0', 'angle'=>0,],
			['text'=>'张冰冰','left'=>182,'top'=>205,'fontSize'=>18,'fontColor'=>'255,0,0','angle'=>0,]
		],
		//图片
		'image'=> [
			['url'     => 'http://local.l56.cn/img/qrcode.png','stream'  => 0,'left'    => 380,'top'     => -380,'right'   => 0,'bottom'  => 0,'width'   => 260,'height'  => 260,'opacity' => 100,'radius' => 100,],
			['url'=>'http://local.l56.cn/img/mendian.jpg','left'=>320,'top'=>70,'right'=>0,'stream'=>0,'bottom'=>0,'width'=>155,'height'=>155,'opacity'=>100,],
		],
		//背景图
		'background' => ['bg'=>'http://local.l56.cn/img/bg.png']
	];

	$filename =dirname(dirname(dirname(dirname(__FILE__)))).'/public/img/'. time() . '.png';

	BaseLib::create_poster($config,$filename);

```

<br/>


##### api返回信息
###### 默认的返回结构

> 正确返回 BaseLib::success();

名称 | 是否必须 | 备注
:---: | :---: | :---:
data | true | 数组，例：['info'=>'','msg'=>'','data'=>'']，info和msg可以不传，默认：使用的config/errorInfo.php中的public_succ。修改的话用msg传public_add_fail。info中传提示信息

```
$data=['id'=>1,'name'=>'张飞'];
例1：BaseLib::success([data'=>$data]);
例2：BaseLib::success(['msg'=>'public_add_fail','info'=>'成功',data'=>$data]);

```
<br/>

> 错误返回 BaseLib::error();

名称 | 是否必须 | 备注
:---: | :---: | :---:
data | true | 数组，例：['info'=>'','msg'=>'','data'=>'']，info和msg可以不传，默认：使用的config/errorInfo.php中的public_succ。修改的话用msg传public_add_fail。info中传提示信息

```
$data=['id'=>1,'name'=>'张飞'];
例1：BaseLib::error([data'=>$data]);
例2：BaseLib::error(['msg'=>'public_add_fail','info'=>'错误',data'=>$data]);

```

<br/>

###### 自定义的的返回结构
```
成功 BaseLib::return_msg();

失败 BaseLib::return_err();

1.可以在配置文件中修改返回结构体return_succ,return_err
2.参数根据配置中的结构体传值，必须是数组

```

<br/>

> 调用方法：类名::函数名 ，也可以BaseLib::result()->success()这种方法调用也可以

```

例：BaseLib::result()->error();BaseLib::result()->return_msg();

```

<br/>


###### 参数验证


> BaseLib::goCheck();

名称 | 是否必须 | 备注
:---: | :---: | :---:
rule | true | 验证规则
message | true | 错误信息

```

成功：['state'=>'ok','info'=>'']
失败：['state'=>'err','info'=>'错误信息']

```
<br/>

> BaseLib::params->goCheck()参数同上

<br/>

###### 异常抛出

> BaseLib::handler();

名称 | 是否必须 | 备注
:---: | :---: | :---:
request | true | render函数中的参数
exception | true | render函数中的参数

```

app/Exceptions/Handler.php中的render函数

```

<br/>

> BaseLib::exceptions->handler()参数同上

<br/>

###### jwt-token

> BaseLib::handler();

名称 | 是否必须 | 备注
:---: | :---: | :---:
request | true | render函数中的参数
exception | true | render函数中的参数

```

app/Exceptions/Handler.php中的render函数

```

<br/>

> BaseLib::exceptions->handler()参数同上