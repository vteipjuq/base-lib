<?php

namespace Raymond\BaseLib\Token;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Redis;
use Raymond\BaseLib\Exceptions\Exceptions;


class Token {

	private $request;

	public function __construct(){
		$this->request=new Request;
	}

	//生成token
	public function generateToken(){
		//32个字符串组成一个组随机字符串
		$randChars=$this->getRandChars(32);
		//用三组字符串，进行md5加密
		$timestamp=time();
		//salt 盐（特殊加密字符串）
		$salt=config('baselib.token.token_salt');

		return md5($randChars.$timestamp.$salt);
	}

	/**
	 * 生成令牌
	 * 用户数据和token写入缓存中
	 */
	public function grantToken($cachedData){
		//生成字符串(令牌)
		$key=$this->generateToken();
		$value=json_encode($cachedData);
		$expire_in=config('baselib.token.token_expire_in');
		$token=Redis::set(config('baselib.token.token_prefix').$key,$value);
		//设置有效期
		Redis::expire(config('baselib.token.token_prefix').$key,$expire_in);
		if(!$token){
			return '服务器缓存异常';
		}
		return $key;
	}

	//获取当前令牌中的变量
	public function getCurrentTokenVar($key=""){
		//从http请求头中获取token
		$token=Request::header('token');
		$vars=Redis::get(config('baselib.token.token_prefix').$token);
		if(!$vars){
			return "参数错误";
		}else{
			if(!is_array($vars)){
				$vars=json_decode($vars,true);
			}
			if(empty($key)){
				return $vars;
			}
			//判断数组中是否有这个值
			if(array_key_exists($key, $vars)){
				return $vars[$key];
			}else{
				return "尝试获取的token变量并不存在";
			}
		}
	}

	/*
	 * 对token进行验证签名,正确返回null,错误直接返回提示
	 */
	public function verifyToken($value){
		if(empty($value)){
			return 'token不允许为空';
		}
		$result=Redis::exists($value);
		if($result){
			return 'token已过期或无效token';
		}
		return $result;
	}

	//刷新token
	public function refreshToken(){
		//获取已过期token
		$token=Request::header('token');
		//生成新的token
		$key= $this->generateToken();
		//获取token中存储的数据
		$result=Redis::get(config('baselib.token.token_prefix').$token);
		//存到缓存中
		$token=Redis::set(config('baselib.token.token_prefix').$key,$result);
		//删除已过期token
		Redis::del(config('baselib.token.token_prefix').$token);
		return $key;
	}


	/**
	 * 获取随机字符串
	 * @param  [type] $length [description]
	 * @return [type]         [description]
	 */
	private function getRandChars($length) {
	    $str    = null;
	    $strPol = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz";
	    $max    = strlen($strPol) - 1;
	    for ($i = 0;
	        $i < $length;
	        $i++) {
	        $str .= $strPol[rand(0, $max)];
	    }
	    return $str;
	}


}