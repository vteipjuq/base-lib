<?php
namespace Raymond\BaseLib\ApiDoc;
use ReflectionMethod;

/**
 * 生成文档类
 */
class ApiDoc {

    private $filter_method = ['__construct']; // 要过滤的方法名称

    public function __construct() {

    }

    /**
     * 生成文档
     */
    public function generate_doc($obj) {

        $this->getDirContent();
        return false;

        // 通过类名 Circle 实例化反射类
        $reflectionClass = new \ReflectionClass($obj);
        $methods         = $reflectionClass->getMethods(ReflectionMethod::IS_PUBLIC);
        //遍历所有的方法
        foreach ($methods as $method) {
            $filter = in_array($method->getName(), $this->filter_method);
            if (!$filter) {
                //获取方法的注释
                $doc = $method->getDocComment();
                //解析注释
                $docParser = new DocParser();
                $info      = $docParser->parse($doc);
                dump($info);
            }
        }

    }

    /**
     * 获取某目录下所有子文件和子目录
     * @param  [type] $path 路径
     * @return [type]       [description]
     */
    private function getDirContent($path = '') {

        /*//获取当前文件所在的绝对目录
        // $dir =  dirname(__FILE__);
        $dir = '../app/Http/Controllers/';
        dump($dir);
        //扫描文件夹
        $file = scandir($dir);
        //显示
        echo " <pre>";
        print_r($file);*/

        $path = '../app/Http/Controllers/';
        $path  = dir_path($path);
        dump($path);
        $files = glob($path . '*');
        dump($files);
        foreach ($files as $v) {
            if (!$exts || preg_match("/\.(.php)/i", $v)) {
                $list[] = $v;
                if (is_dir($v)) {
                    $list = dir_list($v, $exts, $list);
                }
            }
        }
        return $list;
    }

}