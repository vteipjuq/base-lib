<?php

namespace Raymond\BaseLib;
use Raymond\BaseLib\Common\Common;
use Raymond\BaseLib\Common\SharePoster;
use Raymond\BaseLib\Result\Result;
use Raymond\BaseLib\ParamsValidate\ParamsValidate;
use Raymond\BaseLib\Exceptions\Exceptions;
use Raymond\BaseLib\Token\Token;
use Raymond\BaseLib\ApiDoc\ApiDoc;
use Raymond\BaseLib\WeChat\WeChatA;


class BaseLib {

	public function __construct(){

	}


	/*========================== 常用公共函数调用 Common.php ==============================*/

	//用new方法获取common函数
	public static function common(){
		return new Common;
	}

	public static function get_last_time($time){
		$common=new Common;
		return $common->get_last_time($time);
	}

	public static function is_phone($phone){
		$common=new Common;
		return $common->is_phone($phone);
	}

	public static function substr_mobile($phone){
		$common=new Common;
		return $common->substr_mobile($phone);
	}

	public static function http_get($url){
		$common=new Common;
		return $common->http_get($url);
	}

	public static function http_post($url,$data){
		$common=new Common;
		return $common->http_post($url,$data);
	}

	public static function emoji_encode($str){
		$common=new Common;
		return $common->emoji_encode($str);
	}

	public static function emoji_decode($str){
		$common=new Common;
		return $common->emoji_decode($str);
	}

	public static function is_english($str){
		$common=new Common;
		return $common->is_english($str);
	}

	public static function subtext($str){
		$common=new Common;
		return $common->subtext($str);
	}

	public static function get_str_truncate($str){
		$common=new Common;
		return $common->get_str_truncate($str);
	}

	public static function random($str){
		$common=new Common;
		return $common->random($str);
	}

	public static function is_arraynull($str){
		$common=new Common;
		return $common->is_arraynull($str);
	}

	public static function gbk($str){
		$common=new Common;
		return $common->gbk($str);
	}

	public static function utf8($str){
		$common=new Common;
		return $common->utf8($str);
	}

	public static function match_email($str){
		$common=new Common;
		return $common->match_email($str);
	}

	public static function str_exists($str){
		$common=new Common;
		return $common->str_exists($str);
	}

	public static function is_badword($str){
		$common=new Common;
		return $common->is_badword($str);
	}

	public static function get_json_to_arr($str){
		$common=new Common;
		return $common->get_json_to_arr($str);
	}

	public static function has_unsafeword($str){
		$common=new Common;
		return $common->has_unsafeword($str);
	}

	public static function is_wxbrowser($str){
		$common=new Common;
		return $common->is_wxbrowser($str);
	}

	public static function is_wx_company($str){
		$common=new Common;
		return $common->is_wx_company($str);
	}

	public static function string_equal($str){
		$common=new Common;
		return $common->string_equal($str);
	}

	public static function get_random($str){
		$common=new Common;
		return $common->get_random($str);
	}

	public static function zip($str){
		$common=new Common;
		return $common->zip($str);
	}

	public static function unzip($str){
		$common=new Common;
		return $common->unzip($str);
	}

	public static function array_unset_repeat($str){
		$common=new Common;
		return $common->array_unset_repeat($str);
	}

	public static function array_restruct($str){
		$common=new Common;
		return $common->array_restruct($str);
	}

	public static function for_in_array($str){
		$common=new Common;
		return $common->for_in_array($str);
	}

	public static function get_ip($str){
		$common=new Common;
		return $common->get_ip($str);
	}

	public static function generate_password($str){
		$common=new Common;
		return $common->generate_password($str);
	}

	public static function is_password($str){
		$common=new Common;
		return $common->is_password($str);
	}

	public static function rmb_format($str){
		$common=new Common;
		return $common->rmb_format($str);
	}

	public static function number_format_plus($str){
		$common=new Common;
		return $common->number_format_plus($str);
	}

	public static function trans_time($str){
		$common=new Common;
		return $common->trans_time($str);
	}

	public static function chk_card($str){
		$common=new Common;
		return $common->chk_card($str);
	}

	public static function get_gender($str){
		$common=new Common;
		return $common->get_gender($str);
	}

	public static function chk_chinese($str){
		$common=new Common;
		return $common->chk_chinese($str);
	}

	public static function arr_xml($str){
		$common=new Common;
		return $common->arr_xml($str);
	}

	public static function xml_arr($str){
		$common=new Common;
		return $common->xml_arr($str);
	}

	public static function check_remaining_days($expire_time){
		$common=new Common;
		return $common->check_remaining_days($expire_time);
	}

	public static function get_extension($filename){
		$common=new Common;
		return $common->get_extension($filename);
	}

	public static function listDirFiles($DirPath){
		$common=new Common;
		return $common->listDirFiles($DirPath);
	}

	public static function make_dir($DirPath){
		$common=new Common;
		return $common->make_dir($DirPath);
	}

	public static function params_filter($request){
		$common=new Common;
		return $common->params_filter($request);
	}

	/*============================ end 公共函数调用 ================================*/




	/*========================= 生成海报 SharePoster.php =============================*/

	public static function create_poster($config,$filename){
		$sharePoster=new SharePoster();
		return $sharePoster->create_poster($config,$filename);
	}

	/*========================= end 生成海报 SharePoster.php =============================*/




	/*========================= api结果返回信息 Result.php =============================*/

	public static function result(){
		return new Result();
	}

	public static function success($params){
		$result=new Result();
		return $result->success($params);
	}
	
	public static function error($params){
		$result=new Result();
		return $result->error($params);
	}
	
	public static function return_msg($params){
		$result=new Result();
		return $result->return_msg($params);
	}
	
	public static function return_err($params){
		$result=new Result();
		return $result->return_err($params);
	}

	/*============================ end api结果返回信息 ================================*/




	/*========================== 参数验证 ParamsValidate.php =============================*/

	//用new方法获取params函数
	public static function params(){
		return new ParamsValidate();
	}

	public static function goCheck_($rule,$message,$sceneArr=[],$scene=""){
		$paramsValidate=new ParamsValidate();
		$res=$paramsValidate->goCheck($rule,$message,$sceneArr,$scene);
		return $res;
	}


	/*============================ end 参数验证 ================================*/




	/*========================== 异常基础函数 Exceptions.php =============================*/

	//用new方法获取exceptions函数
	public static function exceptions(){
		return new Exceptions();
	}

	/**
	 * 重写exceptions handler
	 * @return [type] [description]
	 */
	public static function handler($request,$e){
		$exceptions=new Exceptions();
		return $exceptions->handler($request,$e);
	}

	/*========================== end 异常基础函数 =============================*/



	/*========================== jwt鉴权 Token.php=============================*/

	//用new方法获取token函数
	public static function Token(){
		return new Token();
	}

	public static function grantToken($cachedData=[]){
		$token=new Token();
		return $token->grantToken($cachedData);
	}

	public static function getCurrentTokenVar($key=""){
		$token=new Token();
		return $token->getCurrentTokenVar($key);
	}

	public static function verifyToken($value){
		$token=new Token();
		return $token->verifyToken($value);
	}

	public static function refreshToken(){
		$token=new Token();
		return $token->refreshToken();
	}

	/*========================== end jwt鉴权 Token.php =============================*/



	/*========================== start 生成文档 ApiDoc.php =============================*/

	public static function generate_doc($obj){
		$doc=new ApiDoc();
		return $doc->generate_doc($obj);
	}

	/*========================== end 生成文档 ApiDoc.php =============================*/



	/*========================== start 微信小程序类库 WeChat.php =============================*/

	public static function WeChatALib($params){
		$result=new WeChatA($params);
		return $result;
	}

	/*========================== end 微信小程序类库 WeChat.php =============================*/





}