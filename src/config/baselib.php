<?php

return [
	//海报图片保存路径
	'poster_path'=>'BaseException',


	//api成功返回信息
	'return_succ'=>[
		'state'=>'ok',
		'code'=>'200',
		'msg'=>'正确',
		'data'=>[]
	],
	//api失败返回信息
	'return_err'=>[
		'state'=>'err',
		'code'=>'400',
		'msg'=>'错误',
		'data'=>[]
	],


	/**
	 * 常基类配置
	 */
	//异常基类名称
	'exception_base_name'=>'BaseException',


	/**
	 * token配置
	 */
	'token'=>[
		//写入缓存中token前缀
		'token_prefix'=>'_tp',
		//token salt盐（特殊加密字符串）
		'token_salt'=>'asdfkdjike87fk3j2k39',
		//token到期时间(7200等于2小时)
		'token_expire_in'=>7200,
	]






];