<?php
/**
 * 结果集返回
 */
namespace Raymond\BaseLib\Result;

class Result {

	public function __construct(){

	}

	/**
	 * 正确返回
	 */
	public function success($data=[]) {
		$result=$this->return_structure($data);

		return response()->json($result);
	}

	/**
	 * 错误返回
	 */
	public function error($data=[]) {
		$result=$this->return_structure($data,'n');

		return response()->json($result);
	}

	//返回json对象结构
	private function return_structure($params=[],$type='y'){
		if(!isset($params['msg']) || empty($params['msg'])){
			if($type=='y'){
				$params['msg']='public_succ';
			}else{
				$params['msg']='public_fail';
			}
		}
		$config=$this->getConfig($params['msg']);

		$code=$config['code'];
		$info=isset($params['info'])?$params['info']:$config['info'];

		if(isset($params['data'])){
			$data=$params['data'];
		}else{
			unset($params['msg']);
			$data=$params;
		}

		return ['status'=>$type,'code' => $code,'info' => $info,'param' => $data];
	}

	//获取配置中的数据
	private function getConfig($key){
		$info=[
			'code'=>config('errorInfo.'.$key.'.statusCode'),
			'info'=>config('errorInfo.'.$key.'.info')
		];
		return $info;
	}


	/*==================================== api返回数据可以自定义 ====================================*/

	/**
	*  返回成功数据
	 * @param  [array]  $params [数组]
	 * @return [string]       [最终的json数据]
	*/
	public function return_msg($params) {
		//组合数据
		$return_data=$this->combined_data(config("baselib.return_succ"),$params);
		return response()->json($return_data);
	}

	/**
	*  返回失败数据
	 * @param  [array]  $params [数组]
	 * @return [string]       [最终的json数据]
	*/
	public function return_err($params) {
		//组合数据
		$return_data=$this->combined_data(config("baselib.return_err"),$params);
		return response()->json($return_data);
	}

	/**
	 * 组合数据
	 * @param  [type] $return_data 返回的数组结构
	 * @param  [type] $params      参数
	 * @return [type]              [description]
	 */
	public function combined_data($return_data,$params){
		foreach ($return_data as $key => $value) {
			if(isset($params[$key])){
				$return_data[$key]=$params[$key];
			}
		}
		return $return_data;
	}

	/*==================================== end api返回数据可以自定义 ====================================*/

}