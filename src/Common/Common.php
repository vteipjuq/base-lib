<?php
/**
 * 常用函数
 */
namespace Raymond\BaseLib\Common;

class Common {

    public function __construct() {

    }

    /**
     * 计算时间
     * @param  [type] $time 日期
     * @return [type]       字符串
     */
    public function get_last_time($time = NULL) {
        $text = '';
        $time = $time === NULL || $time > time() ? time() : intval($time);
        $t    = time() - $time; //时间差 （秒）
        $y    = date('Y', $time) - date('Y', time()); //是否跨年
        switch ($t) {
        case $t == 0:
            $text = '刚刚';
            break;
        case $t < 60:
            $text = $t . '秒前'; // 一分钟内
            break;
        case $t < 60 * 60:
            $text = floor($t / 60) . '分钟前'; //一小时内
            break;
        case $t < 60 * 60 * 24:
            $text = floor($t / (60 * 60)) . '小时前'; // 一天内
            break;
        case $t < 60 * 60 * 24 * 3:
            $text = floor($time / (60 * 60 * 24)) == 1 ? '1天前' : '2天前'; //昨天和前天
            break;
        default:
            $text = '3天前'; //3天前
            break;
        }

        return $text;
    }

    /**
     * 手机号码验证
     * @param  [type] $phone 手机号
     * @return [type]        布尔值
     */
    public function is_phone($phone) {
        if (preg_match("/^1[3456789][0-9]{9}$/", $phone)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * 手机号隐藏中间4位
     * @param  [type] $mobile 手机号
     * @return [type]         字符串
     */
    public function substr_mobile($mobile) {
        if (empty($mobile)) {
            return "";
        }
        return substr_replace($mobile, '****', 3, 4);
    }
    /**
     * curl get 请求
     * @param $url
     * @return 字符串
     */
    public function http_get($url) {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 500);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_URL, $url);
        $res = curl_exec($curl);
        curl_close($curl);
        return $res;
    }

    /**
     * curl post 请求
     * @param $post_data 参数
     * @param $url url地址
     * @return 字符串
     */
    public function http_post($url, $post_data) {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']); // 模拟用户使用的浏览器
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data);
        $res = curl_exec($curl);
        curl_close($curl);
        return $res;
    }

    //对emoji表情转义
    public function emoji_encode($str) {
        $strEncode = '';

        $length = mb_strlen($str, 'utf-8');

        for ($i = 0; $i < $length; $i++) {
            $_tmpStr = mb_substr($str, $i, 1, 'utf-8');
            if (strlen($_tmpStr) >= 4) {
                $strEncode .= '[[EMOJI:' . rawurlencode($_tmpStr) . ']]';
            } else {
                $strEncode .= $_tmpStr;
            }
        }

        return $strEncode;
    }

    //对emoji表情转反义
    public function emoji_decode($str) {
        $strDecode = preg_replace_callback('|\[\[EMOJI:(.*?)\]\]|', function ($matches) {
            return rawurldecode($matches[1]);
        }, $str);
        return $strDecode;
    }

    //判断字符串是否全是英文
    public function is_english($str) {
        if (preg_match("/^[a-zA-Z\s]+$/", $str)) {
            return true;
        }
        return false;
    }
    /**
     * 字符串截取并且超出显示省略号
     * @param  [type] $text   字符串
     * @param  [type] $length 字符串长度
     * @return [type]         字符串
     */
    public function subtext($text, $length) {
        if (mb_strlen($text, 'utf8') > $length) {
            return mb_substr($text, 0, $length, 'utf8') . ' …';
        }
        return $text;
    }
    /**
     * 字符串截取
     * @param  [type]  $string 字符串
     * @param  integer $length 长度
     * @param  string  $etc    要替换的字符串
     * @return [type]          [description]
     */
    public function get_str_truncate($string, $length = 80, $etc = '') {
        if ($length == 0) {
            return '';
        }
        mb_internal_encoding("UTF-8");
        $string = str_replace("\n", "", $string);
        $strlen = mb_strwidth($string);
        if ($strlen > $length) {
            $etclen = mb_strwidth($etc);
            $length = $length - $etclen;
            $str    = '';
            $n      = 0;
            for ($i = 0; $i < $length; $i++) {
                $c = mb_substr($string, $i, 1);
                $n += mb_strwidth($c);
                if ($n > $length) {break;}
                $str .= $c;
            }
            return $str . $etc;
        } else {
            return $string;
        }
    }
    /**
     * 获取随机字符串
     * @param int $length 长度
     * @param string $type 类型
     * @param int $convert 转换大小写 1大写 0小写
     * @return string
     */
    public function random($length = 10, $type = 'letter', $convert = 0) {
        $config = array(
            'number' => '1234567890',
            'letter' => 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
            'string' => 'abcdefghjkmnpqrstuvwxyzABCDEFGHJKMNPQRSTUVWXYZ23456789',
            'all'    => 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890',
        );

        if (!isset($config[$type])) {
            $type = 'letter';
        }

        $string = $config[$type];

        $code   = '';
        $strlen = strlen($string) - 1;
        for ($i = 0; $i < $length; $i++) {
            $code .= $string{mt_rand(0, $strlen)};
        }
        if (!empty($convert)) {
            $code = ($convert > 0) ? strtoupper($code) : strtolower($code);
        }
        return $code;
    }
    /**
     * 判断数组类型参数是否含有空元素值
     * @param  [type]  $param 数组
     * @return boolean       有空值返回true,否则false;
     */
    public function is_arraynull($param) {
        if (!is_array($param)) {
            return false;
        } else {
            foreach ($param as $key => $value) {
                if ($value != '') {
                    $ret = $this->is_arraynull($value);
                } else {
                    return true;
                }
            }
            return false;
        }
    }
    /**
     * utf-8转gbk
     * @param  [type] $str 字符串
     * @return [type]      字符串
     */
    public function gbk($str) {
        return iconv('utf-8', 'gbk', $str);
    }
    /**
     * gbk转utf8
     * @param  [type] $str 字符串
     * @return [type]      字符串
     */
    public function utf8($str) {
        return iconv('gbk', 'utf-8', $str);
    }
    /**
     * 正则验证邮箱地址
     * @param $email 邮箱
     * @return Bool
     */
    public function match_email($email) {
        if (!preg_match('/^(\w-*\.*)+@(\w-?)+(\.\w{2,})+$/', $email)) {
            return false;
        } else {
            return true;
        }
    }
    /**
     * 查询字符是否存在于某字符串
     * @param $haystack 字符串
     * @param $needle 要查找的字符
     * @return bool
     */
    public function str_exists($haystack, $needle) {
        return !(strpos($haystack, $needle) === FALSE);
    }

    /**
     * 检测输入中是否含有特殊字符
     *
     * @param char $string 要检查的字符串名称
     * @return TRUE or FALSE
     */
    public function is_badword($string) {
        $badwords = array("\\", '&', ' ', "'", '"', '/', '*', ',', '<', '>', "\r", "\t", "\n", "#");
        foreach ($badwords as $value) {
            if (strpos($string, $value) !== false) {
                return true;
            }
        }
        return false;
    }
    /**
     * 处理json字符中的特殊字符
     * @param 需要处理的json PHP > 5.2 json_encode 知道转义
     */
    public function get_json_to_arr($value) {
        $escapers     = array("\\", "/", "\"", "\n", "\r", "\t", "\x08", "\x0c");
        $replacements = array("\\\\", "\\/", "\\\"", "\\n", "\\r", "\\t", "\\f", "\\b");
        $result       = str_replace($escapers, $replacements, $value);
        return $result;
    }
    /**
     * 非法字符过滤函数
     * @param 过滤字符串
     */
    public function has_unsafeword($str) {
        $regex = "/\/|\~|\!|\@|\#|\\$|\%|\^|\&|\*|\(|\)|\_|\+|\{|\}|\:|\<|\>|\?|\[|\]|\.|\/|\;|\'|\`|\=|\\\|\|/";
        return preg_replace($regex, "", $str);
    }
    /**
     * 判断是否是微信浏览器还是企业微信浏览器
     */
    public function is_wxbrowser() {
        if (strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') !== false) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 判断是否是企业微信浏览器
     */
    public function is_wx_company() {
        if (strpos($_SERVER['HTTP_USER_AGENT'], 'wxwork') !== false) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 判断两个字符串是否相等
     */
    public function string_equal($str1, $str2) {
        if (strcmp($str1, $str2) == 0) {
            return true;
        } else {
            return false;
        }
    }
    /**
     * 随机验证码
     * @param int $len 生成随机数长度，默认为6
     * @return string
     */
    //随机数
    public function get_random($length = 6) {
        //取10的长度减1次方的值，例：length为6 10的5次方 100000
        $min = pow(10, ($length - 1));
        //取10的长度次方的值，例：length为6 10的6次方减1 999999
        $max = pow(10, $length) - 1;
        //获取并返回该范围的数
        return mt_rand($min, $max);
    }
    /**
     * 压缩文件
     * @param array $files 待压缩文件 array('d:/test/1.txt'，'d:/test/2.jpg');【文件地址为绝对路径】
     * @param string $filePath 输出文件路径 【绝对文件地址】 如 d:/test/new.zip
     * @return string|bool
     */
    public function zip($files, $filePath) {
        //检查参数
        if (empty($files) || empty($filePath)) {
            return false;
        }
        //压缩文件
        $zip = new ZipArchive();
        $zip->open($filePath, ZipArchive::CREATE);
        foreach ($files as $key => $file) {
            //检查文件是否存在
            if (!file_exists($file)) {
                return false;
            }
            $zip->addFile($file, basename($file));
        }
        $zip->close();
        return true;
    }
    /**
     * zip解压方法
     * @param string $filePath 压缩包所在地址 【绝对文件地址】d:/test/123.zip
     * @param string $path 解压路径 【绝对文件目录路径】d:/test
     * @return bool
     */
    public function unzip($filePath, $path) {
        if (empty($path) || empty($filePath)) {
            return false;
        }

        $zip = new ZipArchive();

        if ($zip->open($filePath) === true) {
            $zip->extractTo($path);
            $zip->close();
            return true;
        } else {
            return false;
        }
    }
    /**
     * 二维数组去重
     * @param array $arr 传入数组
     * @param string $key 判断的键值
     * @return array $res
     */
    public function array_unset_repeat($arr, $key) {
        $res = [];
        foreach ($arr as $key => $value) {
            if (isset($res[$value[$key]])) {
                unset($value[$key]);
            } else {
                $res[$value[$key]] = $value;
            }
        }
        return array_values($res);
    }
    /**
     * 二维数组合并重复项
     * @param array $arr 传入数组
     * @param string $key 判断的键值
     * @return array $res
     */
    public function array_restruct($arr, $key) {
        $res = array_column($arr, null, $key);
        foreach ($res as $key => $value) {
            $value = [];
        }
        foreach ($arr as $key => $value) {
            array_push($res[$value[$key]], $value);
        }
        return array_values($res);
    }
    /**
     * 判断字符串是否在数组中 成功返回1失败返回0；
     * @param unknown $str 判定的字符串
     * @param unknown $arr 数组
     * @return number
     */
    public function for_in_array($str, $arr) {
        if (!is_array($arr)) {
            return 0;
        }
        return in_array($str, $arr) ? 1 : 0;
    }
    /**
     * 获取客户端IP
     * @return [type] [description]
     */
    public function get_ip() {
        $strOnlineIp = "";
        if (getenv('HTTP_CLIENT_IP') && strcasecmp(getenv('HTTP_CLIENT_IP'), 'unknown')) {
            $onlineip = getenv('HTTP_CLIENT_IP');
        } elseif (getenv('HTTP_X_FORWARDED_FOR') && strcasecmp(getenv('HTTP_X_FORWARDED_FOR'), 'unknown')) {
            $onlineip = getenv('HTTP_X_FORWARDED_FOR');
        } elseif (getenv('REMOTE_ADDR') && strcasecmp(getenv('REMOTE_ADDR'), 'unknown')) {
            $onlineip = getenv('REMOTE_ADDR');
        } elseif (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] && strcasecmp($_SERVER['REMOTE_ADDR'], 'unknown')) {
            $onlineip = $_SERVER['REMOTE_ADDR'];
        }
        preg_match("/[\d\.]{7,15}/", $onlineip, $onlineipmatches);
        $strOnlineIp = $onlineipmatches[0] ? $onlineipmatches[0] : 'unknown';
        return $strOnlineIp;
    }
    /**
     * 自动生成密码
     * @param  integer $length   密码长度
     * @param  integer $strength 1:加入大写字母BDGHJLMNPQRSTVWXZ，2：加入AEUY，4：加入数字，8：加入特殊字符'@#$%'
     * @return [type]            字符串
     */
    public function generate_password($length = 9, $strength = 0) {
        $vowels     = 'aeuy';
        $consonants = 'bdghjmnpqrstvz';
        if ($strength >= 1) {
            $consonants .= 'BDGHJLMNPQRSTVWXZ';
        }
        if ($strength >= 2) {
            $vowels .= "AEUY";
        }
        if ($strength >= 4) {
            $consonants .= '23456789';
        }
        if ($strength >= 8) {
            $vowels .= '@#$%';
        }

        $password = '';
        $alt      = time() % 2;
        for ($i = 0; $i < $length; $i++) {
            if ($alt == 1) {
                $password .= $consonants[(rand() % strlen($consonants))];
                $alt = 0;
            } else {
                $password .= $vowels[(rand() % strlen($vowels))];
                $alt = 1;
            }
        }
        return $password;
    }
    /**
     * 检查密码长度是否符合规定
     *
     * @param   STRING $password
     * @return  TRUE or FALSE
     */
    public function is_password($password) {
        $strlen = strlen($password);
        if ($strlen >= 6 && $strlen <= 20) {
            return true;
        }

        return false;
    }
    /**
     * 人民币数字小写转大写
     * @param string $number 人民币数值
     * @param string $int_unit 币种单位，默认"元"，有的需求可能为"圆"
     * @param bool $is_round 是否对小数进行四舍五入
     * @param bool $is_extra_zero 是否对整数部分以0结尾，小数存在的数字附加0,比如1960.30
     * @return string
     */
    public function rmb_format($money = 0, $int_unit = '元', $is_round = true, $is_extra_zero = false) {
        // 非数字，原样返回
        if (!is_numeric($money)) {
            return $money;
        }
        // 将数字切分成两段
        $parts = explode('.', $money, 2);
        $int   = isset($parts[0]) ? strval($parts[0]) : '0';
        $dec   = isset($parts[1]) ? strval($parts[1]) : '';
        // 如果小数点后多于2位，不四舍五入就直接截，否则就处理
        $dec_len = strlen($dec);
        if (isset($parts[1]) && $dec_len > 2) {
            $dec = $is_round ? substr(strrchr(strval(round(floatval("0." . $dec), 2)), '.'), 1) : substr($parts[1], 0, 2);
        }
        // 当number为0.001时，小数点后的金额为0元
        if (empty($int) && empty($dec)) {
            return '零';
        }
        // 定义
        $chs     = ['0', '壹', '贰', '叁', '肆', '伍', '陆', '柒', '捌', '玖'];
        $uni     = ['', '拾', '佰', '仟'];
        $dec_uni = ['角', '分'];
        $exp     = ['', '万'];
        $res     = '';
        // 整数部分从右向左找
        for ($i = strlen($int) - 1, $k = 0; $i >= 0; $k++) {
            $str = '';
            // 按照中文读写习惯，每4个字为一段进行转化，i一直在减
            for ($j = 0; $j < 4 && $i >= 0; $j++, $i--) {
                // 非0的数字后面添加单位
                $u   = $int{$i} > 0 ? $uni[$j] : '';
                $str = $chs[$int{$i}] . $u . $str;
            }
            // 去掉末尾的0
            $str = rtrim($str, '0');
            // 替换多个连续的0
            $str = preg_replace("/0+/", "零", $str);
            if (!isset($exp[$k])) {
                // 构建单位
                $exp[$k] = $exp[$k - 2] . '亿';
            }
            $u2  = $str != '' ? $exp[$k] : '';
            $res = $str . $u2 . $res;
        }
        // 如果小数部分处理完之后是00，需要处理下
        $dec = rtrim($dec, '0');
        // 小数部分从左向右找
        if (!empty($dec)) {
            $res .= $int_unit;
            // 是否要在整数部分以0结尾的数字后附加0，有的系统有这要求
            if ($is_extra_zero) {
                if (substr($int, -1) === '0') {
                    $res .= '零';
                }
            }
            for ($i = 0, $cnt = strlen($dec); $i < $cnt; $i++) {
                // 非0的数字后面添加单位
                $u = $dec{$i} > 0 ? $dec_uni[$i] : '';
                $res .= $chs[$dec{$i}] . $u;
                if ($cnt == 1) {
                    $res .= '整';
                }

            }
            // 去掉末尾的0
            $res = rtrim($res, '0');
            // 替换多个连续的0
            $res = preg_replace("/0+/", "零", $res);
        } else {
            $res .= $int_unit . '整';
        }
        return $res;
    }
    /**
     * 返回格式化数字
     * @param int $number 待格式化数字
     * @param int $decimals 保留小数位数，默认2位
     * @param string $dec_point 整数和小数分隔符号
     * @param string $thousands_sep 整数部分每三位数读分隔符号
     * @return string
     */
    public function number_format_plus($number = 0, $decimals = 2, $dec_point = '.', $thousands_sep = ',') {
        $format_num = '0.00';
        if (is_numeric($number)) {
            $format_num = number_format($number, $decimals, $dec_point, $thousands_sep);
        }
        return $format_num;
    }
    /**
     * 转换时间戳为常用的日期格式
     * @param  [type] $timestamp 时间戳
     * @return [type]            [description]
     */
    public function trans_time($timestamp) {
        if ($timestamp < 1) {
            return '无效的Unix时间戳';
        } else {
            return date("Y-m-d H:i:s", $timestamp);
        }
    }
    /**
     * 银行卡号格式检测
     * @param  [type] $card 卡号
     */
    public function chk_card($card) {
        $arr_no = str_split($card);
        $last_n = $arr_no[count($arr_no) - 1];
        krsort($arr_no);
        $i     = 1;
        $total = 0;
        foreach ($arr_no as $n) {
            if ($i % 2 == 0) {
                $ix = $n * 2;
                if ($ix >= 10) {
                    $nx = 1 + ($ix % 10);
                    $total += $nx;
                } else {
                    $total += $ix;
                }
            } else {
                $total += $n;
            }
            $i++;
        }
        $total -= $last_n;
        $total *= 9;
        return $last_n == ($total % 10);
    }
    /**
     * 判断是否为合法的身份证号码
     * @param $mobile
     * @return int
     */
    public function isCreditNo($vStr) {
        $vCity = array(
            '11', '12', '13', '14', '15', '21', '22',
            '23', '31', '32', '33', '34', '35', '36',
            '37', '41', '42', '43', '44', '45', '46',
            '50', '51', '52', '53', '54', '61', '62',
            '63', '64', '65', '71', '81', '82', '91',
        );
        if (!preg_match('/^([\d]{17}[xX\d]|[\d]{15})$/', $vStr)) {
            return false;
        }

        if (!in_array(substr($vStr, 0, 2), $vCity)) {
            return false;
        }

        $vStr    = preg_replace('/[xX]$/i', 'a', $vStr);
        $vLength = strlen($vStr);
        if ($vLength == 18) {
            $vBirthday = substr($vStr, 6, 4) . '-' . substr($vStr, 10, 2) . '-' . substr($vStr, 12, 2);
        } else {
            $vBirthday = '19' . substr($vStr, 6, 2) . '-' . substr($vStr, 8, 2) . '-' . substr($vStr, 10, 2);
        }
        if (date('Y-m-d', strtotime($vBirthday)) != $vBirthday) {
            return false;
        }

        if ($vLength == 18) {
            $vSum = 0;
            for ($i = 17; $i >= 0; $i--) {
                $vSubStr = substr($vStr, 17 - $i, 1);
                $vSum += (pow(2, $i) % 11) * (($vSubStr == 'a') ? 10 : intval($vSubStr, 11));
            }
            if ($vSum % 11 != 1) {
                return false;
            }

        }
        return true;
    }
    /**
     *根据身份证号计算性别
     **/
    public function get_gender($id) {
        //15位数的身份证最后一位是双数就是女的，单数就是男的
        //18位数的身份证倒数第二位（第17位）是双数就是女的，单数就是男的
        $len    = strlen($id);
        $gender = '男';
        $last   = $len == 15 ? substr($id, 0, -1) : ($len == 18 ? substr($id, -2, 1) : 1);
        if ($last % 2 == 0) {
            $gender = '女';
        }
        return $gender;
    }
    /**
     * 检测姓名中文格式
     */
    public function chk_chinese($name) {
        if (preg_match("/^[\x{4e00}-\x{9fa5}]+$/u", $name)) {
            return true;
        }
        return false;
    }
    /**
     * 将数组转换成xml
     * @author ganyuanjiang  <3164145970@qq.com>
     * @createtime 2017-08-31 16:04:13
     * @return xml
     */
    public function arr_xml($arr) {
        if (!is_array($arr) || count($arr) <= 0) {
            return false;
        }
        $xml = "<xml>";
        foreach ($arr as $key => $val) {
            if (is_numeric($val)) {
                $xml .= "<" . $key . ">" . $val . "</" . $key . ">";
            } else {
                $xml .= "<" . $key . "><![CDATA[" . $val . "]]></" . $key . ">";
            }
        }
        $xml .= "</xml>";
        return $xml;
    }
    /**
     * 将xml转为array
     * @author ganyuanjiang  <3164145970@qq.com>
     * @createtime 2017-09-11 10:31:31
     * @param string $xml
     * @return array
     */
    public function xml_arr($xml) {
        if (!$xml) {
            return false;
        }
        //将XML转为array
        //禁止引用外部xml实体
        libxml_disable_entity_loader(true);
        $result = json_decode(json_encode(simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA)), true);
        return $result;
    }
    /**
     * 根据过期时间判断剩余的天数
     * @desc   如果为0，则表示活动已经结束
     * @param  $expire_time 时间戳
     * @return float|int
     */
    public function check_remaining_days($expire_time) {
        // 获取当前时间
        $cur_time    = time();
        $expire_time = (int) $expire_time;

        $diff_time            = ($expire_time - $cur_time);
        $remaining_days_count = 0;
        if ($diff_time > 0) {
            // 计算剩余的天数
            $remaining_days_count = ceil($diff_time / (24 * 3600));
        }
        return $remaining_days_count;
    }
    /**
     * 数组编码转换
     *
     * @param  array   $arr          待处理的数组
     * @param  string  $in_charset   输入编码
     * @param  string  $out_charset  输出编码
     * @return array
     */
    public function arr_iconv($arr, $in_charset = 'UTF-8', $out_charset = 'GBK') {
        $arr = eval('return ' . mb_convert_encoding(var_export($arr, true), $out_charset, $in_charset) . ';');
        return $arr;
    }

    /**
     * 获取文件扩展名（后缀）
     * @param  [type] $filename 文件名
     * @return [type]           [description]
     */
    public function get_extension($filename) {
        $myext = substr($filename, strrpos($filename, '.'));
        return str_replace('.', '', $myext);
    }
    /**
     * 列出目录下的文件名
     * @param  [type] $DirPath 目录
     * @return [type]          [description]
     */
    public function listDirFiles($DirPath) {
        $dirs = [];
        if ($dir = opendir($DirPath)) {
            while (($file = readdir($dir)) !== false) {
                if (!is_dir($DirPath . $file)) {
                    $dirs[] = $file;
                }
            }
        }
        return $dirs;
    }
    /**
     * 检查目标文件夹是否存在，如果不存在则自动创建该目录
     *
     * @param       string      folder     目录路径。不能使用相对于网站根目录的URL
     *
     * @return      bool
     */
    public function make_dir($folder) {
        $reval = false;
        if (!file_exists($folder)) {
            /* 如果目录不存在则尝试创建该目录 */
            @umask(0);
            /* 将目录路径拆分成数组 */
            preg_match_all('/([^\/]*)\/?/i', $folder, $atmp);
            /* 如果第一个字符为/则当作物理路径处理 */
            $base = ($atmp[0][0] == '/') ? '/' : '';
            /* 遍历包含路径信息的数组 */
            foreach ($atmp[1] AS $val) {
                if ('' != $val) {
                    $base .= $val;
                    if ('..' == $val || '.' == $val) {
                        /* 如果目录为.或者..则直接补/继续下一个循环 */
                        $base .= '/';
                        continue;
                    }
                } else {
                    continue;
                }
                $base .= '/';
                if (!file_exists($base)) {
                    /* 尝试创建目录，如果创建失败则继续循环 */
                    if (@mkdir(rtrim($base, '/'), 0777)) {
                        @chmod($base, 0777);
                        $reval = true;
                    }
                }
            }
        } else {
            /* 路径已经存在。返回该路径是不是一个目录 */
            $reval = is_dir($folder);
        }
        clearstatcache();
        return $reval;
    }

    /**
     * 滤器laravel request中请求参数的数组中的多余参数
     * @param  [type] $request request
     * @param  [type] $key 要删除的数组下标（可以不传）
     * @return [type]          [description]
     */
    public function params_filter($request, $key = []) {
        $params = $request->all();
        unset($params[$request->path()], $params['_token']);
        if (count($key) > 0) {
            foreach ($key as $k => $value) {
                unset($params[$value]);
            }
        }
        return $params;
    }

}