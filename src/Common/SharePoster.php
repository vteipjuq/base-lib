<?php
/**
 * 生成微信分享二维码图片
 */
namespace Raymond\BaseLib\Common;

class SharePoster {

	/*
	 * 使用示例一：生成带有二维码的海报
		$config = [
			'image'=>[
				//二维码资源
				['url'=>'qrcode/qrcode.png','stream'=>0,'left'=>116,'top'=>-216,'right'=>0,'bottom'=>0,'width'=>178,'height'=>178,'opacity'=>100]
			],
			//背景图
			'background'=>['bg/bg1.jpg']
		];
		$filename = 'bg/'.time().'.jpg';

		//echo createPoster($config,$filename);
		echo createPoster($config);
	 */

	/*
	 * 使用示例二：生成带有图像，昵称和二维码的海报
		$config = [
			//文字
			'text'=>[
				['text'=>'昵称','left'=>182,'top'=>105,'fontSize'=>18,'fontColor'=>'255,0,0', 'angle'=>0,],
				['text'=>'张冰冰','left'=>182,'top'=>205,'fontSize'=>18,'fontColor'=>'255,0,0','angle'=>0,]
			],
			//图片
			'image'=> [
				['url'     => 'http://local.l56.cn/img/qrcode.png','stream'  => 0,'left'    => 380,'top'     => -380,'right'   => 0,'bottom'  => 0,'width'   => 260,'height'  => 260,'opacity' => 100,'radius' => 100,],
				['url'=>'http://local.l56.cn/img/mendian.jpg','left'=>320,'top'=>70,'right'=>0,'stream'=>0,'bottom'=>0,'width'=>155,'height'=>155,'opacity'=>100,],
			],
			//背景图
			'background' => ['bg'=>'http://local.l56.cn/img/bg.png']
		];
		$filename = 'qrcode/'.time().'.jpg';
	 */

	/**
	 * 生成宣传海报
	 * @param array  参数,包括图片和文字
	 * @param string  $filename 生成海报文件名,不传此参数则不生成文件,直接输出图片
	 * @return [type] [description]
	 */
	public function create_poster($config = [], $filename = "") {
		//如果要看报什么错，可以先注释调这个header
		if (empty($filename)) {
			header("content-type: image/png");
		}

		$imageDefault = array(
			'left'    => 0,
			'top'     => 0,
			'right'   => 0,
			'bottom'  => 0,
			'width'   => 100,
			'height'  => 100,
			'opacity' => 100,
			'radius' => 0,
		);
		$textDefault = array(
			'text'      => '',
			'left'      => 0,
			'top'       => 0,
			'fontSize'  => 32, //字号
			'fontColor' => '255,255,255', //字体颜色
			'angle'     => 0,
			//字体路径
			'fontPath'     => dirname(dirname(__FILE__)) . '/stitac/font/SourceHanSansCN-Normal.ttf',
		);
		$background = $config['background']['url']; //海报最底层得背景
		//背景方法
		$backgroundInfo   = getimagesize($background);
		$backgroundFun    = 'imagecreatefrom' . image_type_to_extension($backgroundInfo[2], false);
		$background       = $backgroundFun($background);
		$backgroundWidth  = imagesx($background); //背景宽度
		$backgroundHeight = imagesy($background); //背景高度
		$background = $this->radius_img($config['background']['url'],isset($config['background']['radius'])?$config['background']['radius']:0);
		$imageRes         = imageCreatetruecolor($backgroundWidth, $backgroundHeight);
		// $color            = imagecolorallocate($imageRes, 0, 0, 0);
		// 透明
		imagesavealpha($imageRes, true);
		//拾取一个完全透明的颜色,最后一个参数127为全透明
		$color = imagecolorallocatealpha($imageRes, 255, 255, 255, 127);
		imagealphablending($imageRes, true);

		imagefill($imageRes, 0, 0, $color);
		// imageColorTransparent($imageRes, $color);  //颜色透明
		imagecopyresampled($imageRes, $background, 0, 0, 0, 0, imagesx($background), imagesy($background), imagesx($background), imagesy($background));
		//处理了图片
		if (!empty($config['image'])) {
			foreach ($config['image'] as $key => $val) {
				$val      = array_merge($imageDefault, $val);
				$info     = getimagesize($val['url']);
				/*$function = 'imagecreatefrom' . image_type_to_extension($info[2], false);
				if ($val['stream']) {
					//如果传的是字符串图像流
					$info     = getimagesizefromstring($val['url']);
					$function = 'imagecreatefromstring';
				}
				$res       = $function($val['url']);*/

				//处理圆角图片
				$res = $this->radius_img($val['url'],$val['radius']);

				$resWidth  = $info[0];
				$resHeight = $info[1];
				//建立画板 ，缩放图片至指定尺寸
				$canvas = imagecreatetruecolor($val['width'], $val['height']);

				//拾取一个完全透明的颜色,最后一个参数127为全透明
				$imgbg = imagecolorallocatealpha($canvas, 255, 255, 255, 127);

				imagefill($canvas, 0, 0, $imgbg);

				 //圆角透明
				imagecolortransparent($canvas ,$imgbg);//把图片中白色设置为透明色

				//关键函数，参数（目标资源，源，目标资源的开始坐标x,y, 源资源的开始坐标x,y,目标资源的宽高w,h,源资源的宽高w,h）
				imagecopyresampled($canvas, $res, 0, 0, 0, 0, $val['width'], $val['height'], $resWidth, $resHeight);

				$val['left'] = $val['left'] < 0 ? $backgroundWidth - abs($val['left']) - $val['width'] : $val['left'];
				$val['top']  = $val['top'] < 0 ? $backgroundHeight - abs($val['top']) - $val['height'] : $val['top'];
				//放置图像
				imagecopymerge($imageRes, $canvas, $val['left'], $val['top'], $val['right'], $val['bottom'], $val['width'], $val['height'], $val['opacity']); //左，上，右，下，宽度，高度，透明度
			}
		}

		//处理文字
		if (!empty($config['text'])) {
			foreach ($config['text'] as $key => $val) {
				$val             = array_merge($textDefault, $val);
				list($R, $G, $B) = explode(',', $val['fontColor']);
				$fontColor       = imagecolorallocate($imageRes, $R, $G, $B);
				$val['left']     = $val['left'] < 0 ? $backgroundWidth - abs($val['left']) : $val['left'];
				$val['top']      = $val['top'] < 0 ? $backgroundHeight - abs($val['top']) : $val['top'];

				imagettftext($imageRes, $val['fontSize'], $val['angle'], $val['left'], $val['top'], $fontColor, $val['fontPath'], $val['text']);
			}
		}

		//生成图片
		if (!empty($filename)) {
			// $res = imagejpeg($imageRes, $filename, 90); //保存到本地
			$res = imagepng($imageRes, $filename); //保存到本地
			imagedestroy($imageRes);
			if (!$res) {
				return false;
			}
			return $filename;
		} else {
			imagejpeg($imageRes); //在浏览器上显示
			imagedestroy($imageRes);
		}
	}

	/**
	 * 处理圆角图片
	 * @param  string  $imgpath 源图片路径
	 * @param  integer $radius  圆角半径长度默认为15,处理成圆型
	 * @return [type]           [description]
	 */
	public function radius_img($imgpath, $radius = 15) {
		$ext     = pathinfo($imgpath);
		$src_img = null;
		switch ($ext['extension']) {
		case 'jpg':
			$src_img = imagecreatefromjpeg($imgpath);
			break;
		case 'png':
			$src_img = imagecreatefrompng($imgpath);
			break;
		}
		$wh = getimagesize($imgpath);
		$w  = $wh[0];
		$h  = $wh[1];
		// $radius = $radius == 0 ? (min($w, $h) / 2) : $radius;
		$img = imagecreatetruecolor($w, $h);

		//这一句一定要有
		imagesavealpha($img, true);
		//拾取一个完全透明的颜色,最后一个参数127为全透明
		$bg = imagecolorallocatealpha($img, 255, 255, 255, 127);
		imagefill($img, 0, 0, $bg);
		$r = $radius; //圆 角半径
		for ($x = 0; $x < $w; $x++) {
			for ($y = 0; $y < $h; $y++) {
				$rgbColor = imagecolorat($src_img, $x, $y);
				if (($x >= $radius && $x <= ($w - $radius)) || ($y >= $radius && $y <= ($h - $radius))) {
					//不在四角的范围内,直接画
					imagesetpixel($img, $x, $y, $rgbColor);
				} else {
					//在四角的范围内选择画
					//上左
					$y_x = $r; //圆心X坐标
					$y_y = $r; //圆心Y坐标
					if (((($x - $y_x) * ($x - $y_x) + ($y - $y_y) * ($y - $y_y)) <= ($r * $r))) {
						imagesetpixel($img, $x, $y, $rgbColor);
					}
					//上右
					$y_x = $w - $r; //圆心X坐标
					$y_y = $r; //圆心Y坐标
					if (((($x - $y_x) * ($x - $y_x) + ($y - $y_y) * ($y - $y_y)) <= ($r * $r))) {
						imagesetpixel($img, $x, $y, $rgbColor);
					}
					//下左
					$y_x = $r; //圆心X坐标
					$y_y = $h - $r; //圆心Y坐标
					if (((($x - $y_x) * ($x - $y_x) + ($y - $y_y) * ($y - $y_y)) <= ($r * $r))) {
						imagesetpixel($img, $x, $y, $rgbColor);
					}
					//下右
					$y_x = $w - $r; //圆心X坐标
					$y_y = $h - $r; //圆心Y坐标
					if (((($x - $y_x) * ($x - $y_x) + ($y - $y_y) * ($y - $y_y)) <= ($r * $r))) {
						imagesetpixel($img, $x, $y, $rgbColor);
					}
				}
			}
		}
		return $img;
	}

}