<?php
/**
 * 小程序类库
 */
namespace Raymond\BaseLib\WeChat;
use Illuminate\Support\Facades\Request;
use Raymond\BaseLib\Common\Common;

class WeChatA {

	private $appid=null;
	private $secret=null;

	public function __construct($params=[]){
		//配置
		$this->appid=$params['appid'];
		$this->secret=$params['secret'];

	}

	//获取access_token
	public function getAccessToken(){
		$tokenUrl='https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid='.$this->appid.'&secret='.$this->secret;
		$common=new Common();
		$result=$common->http_get($tokenUrl);
		$resToken=json_encode($result,true);
		return $resToken;
	}

	//获取openid
	public function getOpenid($code){
		$url='https://api.weixin.qq.com/sns/jscode2session?appid='.$this->appid.'&secret='.$this->secret.'&js_code='.$code.'&grant_type=authorization_code';
		$common=new Common();
		$result=$common->http_get($url);
		$res=json_encode($result,true);
		return $res;
	}


}


