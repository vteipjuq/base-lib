<?php

namespace Raymond\BaseLib\Exceptions;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Exceptions extends ExceptionHandler{

	public function __construct(){

	}

	/**
	 * 重写exceptions handler
	 * @return [type] [description]
	 */
	public function handler($request,$e){
		$className=config("baselib.exception_base_name");

		// 只处理自定义的APIException异常
		if($this->getParentClass($e)==$className) {
			$result = [ 
				"result" => $e->result,
				"code"   => $e->code,
				"msg"    => $e->msg
			]; 
			return response()->json($result); 
		}else{
			// 如果config配置debug为true ==>debug模式的话让laravel自行处理 
			if(config('app.debug')){
				//调用父类的render方法
				return parent::render($request, $e); 
			}else{
				$this->code=500;
				$this->msg='服务器内部错误';
				$this->result='error';
			}
		}
		return parent::render($request, $e);
	}

	//获取基类名称
	public function getParentClass($e){
		$str=get_parent_class($e);
		return substr($str,strripos($str,'\\')+1,strlen($str));
	}


}