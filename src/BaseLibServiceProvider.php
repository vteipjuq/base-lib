<?php
namespace Raymond\BaseLib;
use Illuminate\Support\ServiceProvider;



class BaseLibServiceProvider extends ServiceProvider {
	/**
	 * Bootstrap services.
	 *
	 * @return void
	 */
	public function boot() {

		// 复制自定义的文件到config目录
		$this->publishes([
			// dirname(__DIR__) . '/config/baselib.php' => config_path('baselib.php'),
			__DIR__ . '/config/baselib.php' => config_path('baselib.php')
		], 'config');

		$this->app->singleton("baselib", function () {
			return new BaseLib();
		});
	}

	/**
	 * Register services.
	 *
	 * @return void
	 */
	public function register() {
		$this->mergeConfigFrom(
			// dirname(__DIR__) . '/config/baselib.php', 'baselib'
			__DIR__ . '/config/baselib.php', 'baselib'
		);
	}




}